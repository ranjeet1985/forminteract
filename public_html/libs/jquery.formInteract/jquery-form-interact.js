(function ($) {
    $.fn.formInteract = function (options) {
        var self = this;
        self.options = {};
        if (typeof options !== "object") {
            options = {};
        }
        $.extend(true, self.options, $.formInteract.configuration);
        $.extend(true, self.options, options);

        /**
         * Method returns JSON object having configuration.
         * @returns {Object}
         */
        this.getOptions = function () {
            var opt = $.extend(true, {}, self.options);
            return opt;
        };

        /**
         * this function works directly with jquery post method and data selected element are submitted to server with post method.
         * @param {url} url
         * @param {jqXHR function} func
         * @returns {jqXHR}
         */
        this.postAjax = function (url, func) {
            var data = this.get();
            return $.formInteract.post(url, data, func);
        };
        /**
         * this function works directly with jquery post method and data selected element are submitted to server with get method.
         * @param {url} url
         * @param {jqXHR function} func
         * @returns {jqXHR}
         */
        this.getAjax = function (url, func) {
            var data = this.get();
            return $.get(url, data, func);
        };
        /**
         * 
         * @returns {Boolean} if Validation is ok then return true otherwise false;
         */
        this.validate = function (param) {
            var opt = {
                ignoreCustom: false
            };
            if (typeof param === "object") {
                $.extend(true, opt, param);
            }

            var status = true;
            var err = {};
            $(this).each(function () {
                var ele = $(this);
                var key = $.formInteract.getKey(ele, self.options);
                if (!key) {// will return if no declared key found                   
                    return;
                }

                var errorEle = $("[data-error='" + key + "']");
                $.formInteract.setText(errorEle, "", self.options);
                ele.attr(self.options.invalidFlag, "false");

                for (var valid in $.formInteract.validation) {
                    if (opt.ignoreCustom == true && valid === "custom") {//to ignore custom validation
                        continue;
                    }
                    if (!ele.is("[" + $.formInteract.validation[valid].key + "]")) {
                        continue;
                    }
                    if (self.options.returnErrors === true) {

                        if (!$.formInteract.validation[valid].isValid(ele, self.options)) {
                            var message = ele.attr($.formInteract.validation[valid].message);
                            if (!err[key]) {

                                err[key] = [];

                            }
                            if (message) {
                                err[key].push(message);
                            } else {
                                err[key].push("invalid value");
                            }
                        }

                    } else {
                        if (self.options.setError !== null && typeof self.options.setError === "function") {
                            var v = $.formInteract.validation[valid];
                            if (!v.isValid(ele, self.options)) {
                                var mess = (ele.attr(v.message)) ? ele.attr(v.message) : "";
                                self.options.setError(ele, mess);
                                status = false;
                            }
                        } else {
                            status = $.formInteract.setError(status, ele, $.formInteract.validation[valid], self.options);
                        }

                    }

                }

            });
            if (self.options.returnErrors === true) {
                return err;
            }
            return status;
        };

        this.wireEvents = function () {
            $.formInteract.wireEvents(self);
        };
        this.set = function (data) {
            var obj = $.formInteract.normalize(data);
            $(self).each(function () {
                var ele = $(this);
                var key = $.formInteract.getKey(ele, self.options);
                if (!key) {// if any element found with not any attribute among data-key, name and id then it should return;
                    return;
                }
                if (obj[key]) {
                    $.formInteract.setText(ele, obj[key], self.options);
                } else {
                    //set value blank if there is not key found in data
                    // This is useful if we want to reset form or elements;
                    $.formInteract.setText(ele, null, self.options);
                }

            });
        };
        this.setError = function (errors) {
            $(self).each(function () {
                var ele = $(this);
                var key = $.formInteract.getKey(ele, self.options);
                if (!key) {
                    return;
                }
                var data = $.formInteract.normalize(errors);
                var errorEle = $('[data-error="' + key + '"]');
                if (errorEle.length >= 1) {
                    if (data.hasOwnProperty(key)) {
                        $.formInteract.setText(errorEle, data[key], self.options);
                    } else {
                        ele.attr(self.options.invalidFlag, "false");
                        $.formInteract.setText(errorEle, "", self.options);
                    }
                } else {
                    if (data.hasOwnProperty(key)) {
                        ele.parent().addClass(self.options.errorClass);
                    } else {
                        ele.attr(self.options.invalidFlag, "false");
                        ele.parent().removeClass(self.options.errorClass);
                    }
                }

            });
        };
        //get data
        this.get = function () {
            var data = {};
            var radio = {};
            $(self).each(function () {
                var ele = $(this);
                if (ele.attr("data-ignore-get")) {
                    return;
                }
                var key = $.formInteract.getKey(ele, self.options);

                if (!key) {//will return if not key found
                    return;
                }

                //getting value                        
                var val = null;
                var load = ele.attr("data-load-get");
                if (!self.options.load.get[load]) {
                    val = $.formInteract.getText(ele, self.options);
                } else if (typeof self.options.load.get[load] === "function") {
                    val = self.options.load.get[load](ele);
                } else {
                    val = "undefined";
                }
                if (ele.is(":radio")) {
                    if (val === false) { // if element if radio and not selected then not need to process it because its values are not considered
                        return;
                    }
                    val = ele.val();
                }
                if (val === null) {
                    val = "";
                }
                if (self.options.replaceParent === true && typeof val === "object") {
                    if (key.lastIndexOf(".") === -1) {
                        for (var k in val) {
                            data[key] = val[k];
                        }
                    } else {
                        var k = key.split(".");
                        var str = key.substring(0, key.lastIndexOf("."));
                        for (var nk in val) {
                            data[str + "." + nk] = val[nk];
                        }
                    }

                } else {
                    data[key] = val;
                }

            });
            if (self.options.chaining === true) {
                return $.formInteract.objectify(data);
            }
            return data;
        };
        return this;
    };
    $.formInteract = {};
    $.formInteract.log = function (message) {
        throw message;
    };
    /**
     * 
     * @param {JSON Object} object
     * @returns {JSON Object} object
     */
    $.formInteract.objectify = function (object) {
        if (typeof object !== "object") {
            $.formInteract.log("There must be a object to objectify.");
        }
//        var self = this;
        var createObject = function (key, value) {
            var data = {};
            if (key.lastIndexOf(".") === -1) {
                data[key] = value;
            } else {
                var k = key.split(".");
                var passKey = key.substr((k[0].length) + 1);
                data[k[0]] = createObject(passKey, value);
            }
            return data;
        };
        var data = {};
        for (var key in object) {
            if (key.lastIndexOf(".") === -1) {
                data[key] = object[key];
            } else {
                var k = key.split(".");
                var passKey = key.substr((k[0].length) + 1);
                var d = {};
                d[k[0]] = createObject(passKey, object[key]);
                $.extend(true, data, d);
            }
        }
        return data;

    };
    /**
     * 
     * @param {type} object a JSON object is required which will be normalized.
     * @returns {type} object a JSON object having values normalized from object 
     */
    $.formInteract.normalize = function (object) {
        var arr = {};
        for (var key in object) {
            var o = {};
            if (typeof object[key] !== "object") {
                arr[key] = object[key];
            } else {
                var ret = $.formInteract.normalize(object[key]);
                for (var a in ret) {
                    arr[key + "." + a] = ret[a];
                }
            }
        }
        return arr;
    };
    /**
     * This function is just like post method of jquery but adds ajax loadings
     * @param {String} url
     * @param {Object} data
     * @param {function} func
     * @returns {jQueryPOST}
     */
    $.formInteract.post = function (url, data, func) {
        var ele = document.createElement("div");
        var ajax = document.createElement("div");
        try {
            ele.className = "ajax-screen-dasabled";
            ajax.className = "ajax-centered";
            $(ele).appendTo("body");
            $(ajax).appendTo("body");
            var post = $.post(url, data, func).done(function () {
                $(ele).replaceWith("");
                $(ajax).replaceWith("");
            }).complete(function () {
                $(ele).replaceWith("");
                $(ajax).replaceWith("");
            });
            return post;

        } catch (e) {
            $(ele).replaceWith("");
            $(ajax).replaceWith("");
            $.formInteract.log(e);
        }

    };
    $.formInteract.resolveAttributeName = function (attr) {
        return attr.split(".").join("\\.").split("]").join("\\]").split("[").join("\\[");

    };
    $.formInteract.getAttributeValue = function (attr, options) {
        if (attr.lastIndexOf("#") === -1) {
            return attr;
        }
        return $.formInteract.getText($($.formInteract.resolveAttributeName(attr)), options);
    };
    $.formInteract.calculation = {};

    $.formInteract.resolveAttribute = function (attributeValue, options) {
        var ret = {};
        var reg = /\*|\+|\-\-|\/|\%/;
        var val = attributeValue.split(reg);
        ret.elementId = [];
        if (val[0].lastIndexOf("#") !== -1) {
            var at = $.formInteract.resolveAttributeName(val[0]).split(reg)[0].toString();
            ret.elementId.push(at);
        }

        if (val.length === 1) {
            return ret;
        }

        if (val[1].lastIndexOf("#") !== -1) {
            var at = $.formInteract.resolveAttributeName(val[1]).split(reg)[0].toString();
            ret.elementId.push(at);
        }

        if (attributeValue.lastIndexOf("/") >= 1) {
            ret.fun = function () {
                var val1 = $.formInteract.getAttributeValue(val[0], options) * 1;
                var val2 = $.formInteract.getAttributeValue(val[1], options) * 1;
                return val1 / val2;
            };
        }
        if (attributeValue.lastIndexOf("*") >= 1) {
            ret.fun = function () {
                var val1 = $.formInteract.getAttributeValue(val[0], options) * 1;
                var val2 = $.formInteract.getAttributeValue(val[1], options) * 1;
                return val1 * val2;
            };
        }
        if (attributeValue.lastIndexOf("+") >= 1) {
            ret.fun = function () {
                var val1 = $.formInteract.getAttributeValue(val[0], options) * 1;
                var val2 = $.formInteract.getAttributeValue(val[1], options) * 1;
                return val1 + val2;
            };
        }
        if (attributeValue.lastIndexOf("--") >= 1) {
            ret.fun = function () {
                var val1 = $.formInteract.getAttributeValue(val[0], options) * 1;
                var val2 = $.formInteract.getAttributeValue(val[1], options) * 1;
                return val1 - val2;
            };
        }
        if (attributeValue.lastIndexOf("%") >= 1) {
            ret.fun = function () {
                var val1 = $.formInteract.getAttributeValue(val[0], options) * 1;
                var val2 = $.formInteract.getAttributeValue(val[1], options) * 1;
                return val1 * val2 / 100;
            };
        }
        return ret;

    };

    $.formInteract.events = {
        "data-bind-change-value": {
            do: function (ele, options, key) {
                var refs = $.formInteract.resolveAttribute(ele.attr(key), options);
                var otherEle = $(refs.elementId[0]);
                if (otherEle.length === 1) {
                    otherEle.bind("change", function () {
                        var val = "";
                        if (refs.fun) {
                            val = refs.fun();
                        } else {
                            var val = $.formInteract.getText(otherEle2, options);
                        }
                        $.formInteract.setText(ele, val, options);
                    });
                } else if (refs.elementId[0].lastIndexOf("#") >= 0) {
                    $.formInteract.log("Element does not found with id::" + refs.elementId[0]);
                    return;
                }
                if (refs.elementId.length <= 1) {
                    return;
                }

                var otherEle2 = $(refs.elementId[1]);
                if (otherEle2.length === 1) {
                    otherEle2.bind("change", function () {
                        var val = "";
                        if (refs.fun) {
                            val = refs.fun();
                        } else {
                            var val = $.formInteract.getText(otherEle2, options);
                        }
                        $.formInteract.setText(ele, val, options);
                    });
                } else if (refs.elementId[1].lastIndexOf("#") >= 0) {
                    $.formInteract.log("Element does not found with id::" + refs.elementId[1]);
                    return;
                }
            }
        },
        "data-bind-keyUp-value": {
            do: function (ele, options, key) {
                var refs = $.formInteract.resolveAttribute(ele.attr(key), options);
                var otherEle = $(refs.elementId[0]);
                if (otherEle.length === 1) {
                    otherEle.bind("keyup", function () {
                        var val = "";
                        if (refs.fun) {
                            val = refs.fun();
                        } else {
                            var val = $.formInteract.getText(otherEle2, options);
                        }
                        $.formInteract.setText(ele, val, options);
                    });
                } else if (refs.elementId[0].lastIndexOf("#") >= 0) {
                    $.formInteract.log("Element does not found with id::" + refs.elementId[0]);
                    return;
                }
                if (refs.elementId.length <= 1) {
                    return;
                }
                var otherEle2 = $(refs.elementId[1]);
                if (otherEle2.length === 1) {
                    otherEle2.bind("keyup", function () {
                        var val = "";
                        if (refs.fun) {
                            val = refs.fun();
                        } else {
                            var val = $.formInteract.getText(otherEle2, options);
                        }
                        $.formInteract.setText(ele, val, options);
                    });
                } else if (refs.elementId[1].lastIndexOf("#") >= 0) {
                    $.formInteract.log("Element does not found with id::" + refs.elementId[1]);
                    return;
                }
            }
        },
        "data-bind-change-validation": {
            do: function (ele, options, key) {
                var form = ele.formInteract();
                ele.bind("change", function () {
                    form.validate();
                });
            }
        },
        "data-bind-keyUp-validation": {
            do: function (ele, options, key) {
                var form = ele.formInteract();
                ele.bind("keyup", function () {
                    form.validate();
                });
            }
        }
    };
    $.formInteract.validation = {
        notNull: {
            key: "data-validation-notNull",
            message: "data-validation-notNull-message",
            isValid: function (ele, ___options) {
                if ($.formInteract.getText(ele, ___options) === null) {
                    return false;
                }
                return true;
            }
        },
        assertTrue: {
            key: "data-validation-assertTrue",
            message: "data-validation-assertTrue-message",
            isValid: function (ele, ___options) {
                return $.formInteract.getText(ele, ___options) === true;
            }
        },
        equals: {
            key: "data-validation-isEqual",
            message: "data-validation-isEqual-message",
            isValid: function (ele, ___options) {
                var refs = $.formInteract.resolveAttribute(ele.attr(this.key), ___options);
                var otherEle = $(refs.elementId[0]);
                if (otherEle.length !== 1) {
                    $.formInteract.log("could not find the element definded in isEqual on element " + $.formInteract.getKey(ele, ___options));
                }
                var otherValue = $.formInteract.getText(otherEle, ___options);
                var val = $.formInteract.getText(ele, ___options);
                return val === otherValue;
            }
        },
        notEquals: {
            key: "data-validation-notEqual",
            message: "data-validation-notEqual-message",
            isValid: function (ele, ___options) {
                var refs = $.formInteract.resolveAttribute(ele.attr(this.key), ___options);
                var otherEle = $(refs.elementId[0]);
                if (otherEle.length !== 1) {
                    $.formInteract.log("could not find the element definded in notEqual on element " + $.formInteract.getKey(ele, ___options));
                }
                var otherValue = $.formInteract.getText(otherEle, ___options);
                var val = $.formInteract.getText(ele, ___options);
                return val !== otherValue;
            }
        },
        min: {
            key: "data-validation-min",
            message: "data-validation-min-message",
            isValid: function (ele, ___options) {
                var attr = ele.attr(this.key);
                var refs = $.formInteract.resolveAttribute(ele.attr(this.key), ___options);
                if (attr.lastIndexOf("#") === -1) {
                    var val = ele.attr(this.key) * 1;
                    return ($.formInteract.getText(ele, ___options) * 1) >= val;
                } else {

                    var otherEle = $(refs.elementId[0]);
                    var val = $.formInteract.getText(otherEle, ___options) * 1;
                    return ($.formInteract.getText(ele, ___options) * 1) >= val;
                }
            }
        },
        max: {
            key: "data-validation-max",
            message: "data-validation-max-message",
            isValid: function (ele, ___options) {
                var attr = ele.attr(this.key);
                var refs = $.formInteract.resolveAttribute(ele.attr(this.key), ___options);
                if (attr.lastIndexOf("#") === -1) {
                    var val = ele.attr(this.key) * 1;
                    return ($.formInteract.getText(ele, ___options) * 1) <= val;
                } else {
                    var otherEle = $(refs.elementId[0]);
                    var val = $.formInteract.getText(otherEle, ___options) * 1;
                    return ($.formInteract.getText(ele, ___options) * 1) <= val;
                }
            }
        },
        sizeMin: {
            key: "data-validation-sizeMin",
            message: "data-validation-sizeMin-message",
            isValid: function (ele, ___options) {
                var val = ele.attr(this.key) * 1;
                if (!val) {
                    $.formInteract.log("invalid min value to check in element " + $.formInteract.getKey(ele, ___options));
                    return false;
                }
                var text = $.formInteract.getText(ele, ___options);
                if (text === null) {
                    text = "";
                }
                return   text.length >= val;
            }
        },
        sizeMax: {
            key: "data-validation-sizeMax",
            message: "data-validation-sizeMax-message",
            isValid: function (ele, ___options) {
                var val = ele.attr(this.key) * 1;
                if (!val) {
                    $.formInteract.log("invalid max value to check in element " + $.formInteract.getKey(ele, ___options));
                    return false;
                }
                var text = $.formInteract.getText(ele, ___options);
                if (text === null) {
                    text = "";
                }
                return     text.length <= val;
            }
        },
        email: {
            key: "data-validation-email",
            message: "data-validation-email-message",
            isValid: function (ele, ___options) {
                var val = $.formInteract.getText(ele, ___options);
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(val);
            }
        },
        pattern: {
            key: "data-validation-pattern",
            message: "data-validation-pattern-message",
            isValid: function (ele, ___options) {
                var val = $.formInteract.getText(ele, ___options);
                var pat = ele.attr(this.key);
                return pat.test(val);
            }
        },
        between: {
            key: "data-validation-between",
            message: "data-validation-between-message",
            isValid: function (ele, ___options) {
                var range = ele.attr(this.key).split(",");
                if (range.length !== 2) {
                    $.formInteract.log('value of attribute ' + this.key + ' of element ' + $.formInteract.getKey(ele, ___options) + '  must seperated by comma(", ") Example: 100,200 ');
                    return false;
                }
                var val = $.formInteract.getText(ele, ___options) * 1;
                return (val >= (range[0] * 1) && val <= (range[1] * 1));
            }
        },
        /**
         * to check element is not empty and has children elements
         */
        notEmpty: {
            key: "data-validation-notEmpty",
            message: "data-validation-notEmpty-message",
            isValid: function (ele, ___options) {
                var attr = ele.attr(this.key);
                return ele.children(attr).length !== 0;
            }
        },
        custom: {
            key: "data-validation-custom", // value of this attribute must have name of function having two arguments one is element and another is function having rsponse as argument Example {status:1, message"duplicate name"}          
            delay: 400,
            isValid: function (ele, options) {
                var fun = window[ele.attr(this.key)];
                if (!fun) {
                    $.formInteract.log("function not find defined in validation of element ");
                    return false;
                }

                //register time of function called
                var time = new Date().getTime();
                ele.attr($.formInteract.configuration.lastServerValidationAttribute, time);
                setTimeout(function () {
                    var curTime = ele.attr($.formInteract.configuration.lastServerValidationAttribute) * 1;
                    if (curTime === time) {
                        if ($.formInteract.getText(ele, options) === null || $.formInteract.getText(ele, options) === "") {
                            return true;// will not validation if element has not value.                    
                        }

                        fun(ele, function (rsp) {
                            var key = $.formInteract.getKey(ele, options);
                            var errorEle = $('[data-error="' + key + '"]');
                            if (rsp.status === 0) {
                                $.formInteract.setText(errorEle, "", options);
                            } else {
                                $.formInteract.setText(errorEle, rsp.message, options);
                            }
                        });
                    }
                }, this.delay);
                return true;
            }
        }
    };
    $.formInteract.getKey = function (ele, options) {
        if (!options.field) {
            options.field = "auto";
        }
        var key = null;
        if (options.field === "auto") {
            key = ele.attr("data-key");
        } else {
            key = ele.attr(options.field);
        }
        if (!key) {
            if (!ele.attr("name")) {
                if (!ele.attr("id")) {
                    return;
                } else {
                    key = ele.attr("id");
                }
            } else {
                key = ele.attr("name");
            }
        }
        return key;
    };
    $.formInteract.getText = function (element, options) {
        var ret = "";
        if (!options.numerClass) {
            options.numerClass = "numeric";
        }
        if (element.is("input, select, textarea")) {
            if (element.is(":checkbox") || element.is(":radio")) {
                return element.prop("checked");
            } else if (element.hasClass(options.numerClass)) {
                var val = element.val() * 1;
                if (val > 0 || val < 0) {
                    ret = val;
                } else {
                    ret = "";
                }
            } else {
                ret = element.val();
            }

        } else if (element.is("label")) {
            ret = element.text();
        } else {
            ret = element.html();
        }
        if (ret !== null) {
            ret = ret.toString().trim();
        }
        if (ret === "") {
            return null;
        }
        return ret;
    };
    $.formInteract.setText = function (element, value, options) {
        var load = element.attr("data-load-set");
        if (typeof options.load.set[load] === "function") {
            options.load.set[load](element, value);
            return;
        }

        if (element.is("input, select, textarea")) {
            element.val(value);
        } else if (element.is("label")) {
            element.text(value);
        } else {
            element.html(value);
        }
    };
    $.formInteract.setError = function (validFlag, ele, valid, options) {
        var key = $.formInteract.getKey(ele, options);
        var errorEle = $("[data-error='" + key + "']");

        if (!valid.isValid(ele, options)) {
            validFlag = false;
            if (ele.attr(options.invalidFlag) === "true") {
                return;//return if element is already invalidated and need not to put message further.
            }
            ele.attr(options.invalidFlag, "true");

            if (errorEle.length >= 1) {
                var mess = (ele.attr(valid.message)) ? ele.attr(valid.message) : "invalid........";
                $.formInteract.setText(errorEle, mess, options);
            } else {
                ele.parent().addClass(options.errorClass);
            }

        } else {
            if (ele.attr(options.invalidFlag) === "true") {
                //if element is already invalidated then nothing will need to do.
            } else {
                if (errorEle.length >= 1) {
                    $.formInteract.setText(errorEle, "", options);
                } else {
                    ele.parent().removeClass(options.errorClass);
                }
            }
        }
        return validFlag;
    };

    $.formInteract.configuration = {
        field: "auto",
        chaining: false,
        replaceParent: true,
        load: {set: {}, get: {}},
        errorClass: "has-error",
        returnErrors: false,
        numerClass: "numeric",
        lastServerValidationAttribute: "data-validation-server-time",
        setError: null//function (ele, message) {}
        ,
        //validation
        invalidFlag: "data-invalid",
        ajaxAddress: "data-ajax-address",
        ajaxElements: "data-ajax-elements",
        ajaxMethod: "data-ajax-method",
        autoCompleteObjects: {}

    };
    $.formInteract.wireContainerEvents = function (container) {
        for (var key in $.formInteract.events) {
            $.formInteract.wireEvents(container.find('[' + key + ']'));
        }
    };
    $.formInteract.wireEvents = function (elements) {
        for (var key in $.formInteract.events) {
            elements.each(function () {
                var ele = $(this);
                if (ele.is('[' + key + ']')) {
                    $.formInteract.events[key].do(ele, $.formInteract.configuration, key);
                }
            });
        }
    };
    $.formInteract.message = function (message) {
        var mess = "";
        if (typeof message === "object") {
            for (var k in message) {
                mess += "* " + message[k] + "<br/>";
            }
        } else {
            mess = message;
        }
        $.gritter.add({title: "Message", text: mess});
    };

    $.formInteract.getAutoComplete = function (id) {
        return $.formInteract.configuration.autoCompleteObjects[id][0].selectize;
    };

    $.formInteract.wireContainerEvents($("body"));
    $(".autocomplete").each(function () {
        var ele = $(this);
        var address = ele.attr($.formInteract.configuration.ajaxAddress);
        var id = ele.attr("id");
        if (!id) {
            var id = new Date().getTime();
            ele.attr("id", id);
        }
        if (address) {
            var els = ele.attr($.formInteract.configuration.ajaxElements);
            var method = ele.attr($.formInteract.configuration.ajaxMethod);
            if (!method)
                method = "GET";

            $.formInteract.configuration.autoCompleteObjects[id] = ele.selectize({
                valueField: 'key',
                labelField: 'value',
                searchField: 'value',
                create: false,
                load: function (query, callback) {
                    if (!query.length)
                        return callback();
                    var data = $(els).formInteract().get();
                    data.query = query;
//                    alert(JSON.stringify(data));
                    $.ajax({
                        url: address,
                        type: method,
                        data: data,
                        error: function () {
                            callback();
                        },
                        success: function (rsp) {
                            if (rsp.status !== 0) {
                                $.formInteract.message(rsp.message);
                                return;
                            }
                            var selectize = $.formInteract.getAutoComplete(id);
                            selectize.clear();
                            selectize.clearOptions();
                            callback(rsp.message);
                        }
                    });
                }
            });

        } else {
            $.formInteract.configuration.autoCompleteObjects[id] = ele.selectize({valueField: 'key',
                labelField: 'value',
                searchField: 'value',
                create: false});
        }
    });
}(jQuery));
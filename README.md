/* ------------Javascript Code Example-------------- */

            //first we have to initialize the formIntract. this is one time excercise.    
            var form = $("form").find("[name]").formInteract();

            //Run validation on form.
            $("#btn-validate").click(function () {
                //   validate form here and return if it retuns false/ if validation is not successful.
                //   validation will set messages of invalidation in the respective elements   
                if (!form.validate()) {
                    return;
                }
                //do your stuff is form is validated
            });

            //Get your data from form.
            $("#btn-get").click(function () {
                //get data of all selected elements in form intance above as JSON Object.
                var data = form.get();
                //now you data in alert
                alert(JSON.stringify(data));
            });
            
            $("#btn-getPurJSON").click(function () {
                //You can create another instance of same in run time.
                var form2 = $("form").find("[name]").formInteract({chaining: true});
                //get data of all selected elements in form intance above as JSON Object.                                
                var data = form2.get();
                //now you data in alert
                alert(JSON.stringify(data));
            });
            //Set your data to form.
            $("#btn-set").click(function () {
                //have some data in json object
                var data = {
                    employee: {
                        firstName: "Ranjeet",
                        lastName: "Singh",
                        address: "New Delhi",
                        age: 29,
                        email: "ranjeet1985@gmail.com",
                        salary: 10000,
                        rating: 4,
                        percentage: 40
                    }
                };
                //set the JSON data you have to form.
                form.set(data);
            });
            //Reset your form.
            $("#btn-reset").click(function () {
                //To reset your form just pass a blank object to it.
                form.set({});
            });